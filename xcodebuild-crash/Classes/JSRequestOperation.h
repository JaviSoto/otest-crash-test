//
//  JSRequestOperation.h
//  xcodebuild-crash
//
//  Created by Javier Soto on 6/12/12.
//  Copyright (c) 2012 Javier Soto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSRequestOperation : NSObject

+ (void)startRequestWithCompletionBlock:(dispatch_block_t)completion;

@end
