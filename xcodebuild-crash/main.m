//
//  main.m
//  xcodebuild-crash
//
//  Created by Javier Soto on 6/12/12.
//  Copyright (c) 2012 Javier Soto. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JSAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JSAppDelegate class]));
    }
}
