//
//  JSAppDelegate.h
//  xcodebuild-crash
//
//  Created by Javier Soto on 6/12/12.
//  Copyright (c) 2012 Javier Soto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
