//
//  xcodebuild_crashTests.m
//  xcodebuild-crashTests
//
//  Created by Javier Soto on 6/12/12.
//  Copyright (c) 2012 Javier Soto. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

#import "JSRequestOperation.h"

#define TEST_WAIT_UNTIL_TRUE_SLEEP_SECONDS (0.1)
#define TEST_WAIT_UNTIL_TRUE(expr) \
while( (expr) == NO ) [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:TEST_WAIT_UNTIL_TRUE_SLEEP_SECONDS]];

@interface xcodebuild_crashTests : SenTestCase
{
    BOOL _done;
}
@end

@implementation xcodebuild_crashTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testRequest
{
    [JSRequestOperation startRequestWithCompletionBlock:^{
        _done = YES;
    }];
    
    TEST_WAIT_UNTIL_TRUE(_done);
}

@end
