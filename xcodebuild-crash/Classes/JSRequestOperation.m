//
//  JSRequestOperation.m
//  xcodebuild-crash
//
//  Created by Javier Soto on 6/12/12.
//  Copyright (c) 2012 Javier Soto. All rights reserved.
//

#import "JSRequestOperation.h"

#import "AFNetworking.h"

@implementation JSRequestOperation

// Method called from xcodebuild_crashTests.m to show this problem: http://stackoverflow.com/questions/10808229/xcodebuild-crashes-when-running-tests-from-cli
+ (void)startRequestWithCompletionBlock:(dispatch_block_t)completion
{
    // This is a sample URL of a server with an invalid SSL certificate
    NSURL *url = [NSURL URLWithString:[@"https://api-dev.mindsnacks.com" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:@"/" parameters:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    BOOL allowInvalidSSLCertificates = YES;
    
    if (allowInvalidSSLCertificates)
    {
        [operation setAuthenticationChallengeBlock:^(NSURLConnection *connection, NSURLAuthenticationChallenge *challenge) {
            if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
                [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
            }
        }];
        
        [operation setAuthenticationAgainstProtectionSpaceBlock:^BOOL(NSURLConnection *connection, NSURLProtectionSpace *protectionSpace) {
            return YES;
        }];
    }
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, NSData *responseObject) {
        NSLog(@"Success: %@", [[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding] autorelease]);
        completion();
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure %@", error);
        completion();
    }];
    
    [httpClient enqueueHTTPRequestOperation:operation];
    
    [operation release];
    [httpClient release];
}

@end
