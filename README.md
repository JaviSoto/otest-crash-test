# otest crash test

#### otest crash with NSURLConnectionLoader demostration project.

[Stackoverflow question](http://stackoverflow.com/posts/10808229)

> Crash log:


    Process:         otest [49075]
    Path:            /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/Developer/usr/bin/otest
    Identifier:      otest
    Version:         ??? (???)
    Code Type:       X86 (Native)
    Parent Process:  bash [49070]

    Date/Time:       2012-06-12 15:13:50.980 -0700
    OS Version:      Mac OS X 10.7.4 (11E53)
    Report Version:  9

    Crashed Thread:  5  com.apple.NSURLConnectionLoader

    Exception Type:  EXC_BAD_ACCESS (SIGBUS)
    Exception Codes: KERN_PROTECTION_FAILURE at 0x0000000000000000

    VM Regions Near 0:
    --> __PAGEZERO             0000000000000000-0000000000001000 [    4K] ---/--- SM=NUL  /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/Developer/usr/bin/otest
        __TEXT                 0000000000001000-0000000000003000 [    8K] r-x/r-x SM=COW  /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/Developer/usr/bin/otest

    Thread 0:: Dispatch queue: com.apple.main-thread
    0   libsystem_kernel.dylib          0x98908c22 mach_msg_trap + 10
    1   libsystem_kernel.dylib        	0x989081f6 mach_msg + 70
    2   CoreFoundation                	0x009a2b49 __CFRunLoopServiceMachPort + 185
    3   CoreFoundation                	0x009a77db __CFRunLoopRun + 1243
    4   CoreFoundation                	0x009a6ed4 CFRunLoopRunSpecific + 276
    5   CoreFoundation                	0x009a6dab CFRunLoopRunInMode + 123
    6   Foundation                    	0x0001126c -[NSRunLoop(NSRunLoop) runMode:beforeDate:] + 298
    7   Foundation                    	0x0008b9d9 -[NSRunLoop(NSRunLoop) runUntilDate:] + 88
    8   xcodebuild-crashTests         	0x01684cb7 -[xcodebuild_crashTests testRequest] + 247 (xcodebuild_crashTests.m:45)
    9   CoreFoundation                	0x009f4f5d __invoking___ + 29
    10  CoreFoundation                	0x009f4e81 -[NSInvocation invoke] + 305
    11  SenTestingKit                 	0x20103ed1 -[SenTestCase invokeTest] + 219
    12  SenTestingKit                 	0x2010405b -[SenTestCase performTest:] + 183
    13  SenTestingKit                 	0x201037bf -[SenTest run] + 82
    14  SenTestingKit                 	0x2010792b -[SenTestSuite performTest:] + 139
    15  SenTestingKit                 	0x201037bf -[SenTest run] + 82
    16  SenTestingKit                 	0x2010792b -[SenTestSuite performTest:] + 139
    17  SenTestingKit                 	0x201037bf -[SenTest run] + 82
    18  SenTestingKit                 	0x201063ec +[SenTestProbe runTests:] + 174
    19  libobjc.A.dylib               	0x0072e5d6 +[NSObject performSelector:withObject:] + 70
    20  otest                         	0x00002342 0x1000 + 4930
    21  otest                         	0x000025ef 0x1000 + 5615
    22  otest                         	0x0000268c 0x1000 + 5772
    23  otest                         	0x00002001 0x1000 + 4097
    24  otest                         	0x00001f71 0x1000 + 3953

    Thread 1:: Dispatch queue: com.apple.libdispatch-manager
    0   libdispatch.dylib             	0x010ef108 _dispatch_source_invoke + 49
    1   libdispatch.dylib             	0x010ec4f5 _dispatch_queue_drain + 180
    2   libdispatch.dylib             	0x010ec670 _dispatch_mgr_queue_drain + 44
    3   libdispatch.dylib             	0x010ef75d _dispatch_mgr_invoke + 125
    4   libdispatch.dylib             	0x010ef6e0 _dispatch_mgr_thread + 61

    Thread 2:
    0   libsystem_kernel.dylib        	0x9890b02e __workq_kernreturn + 10
    1   libsystem_c.dylib             	0x9aabcccf _pthread_wqthread + 773
    2   libsystem_c.dylib             	0x9aabe6fe start_wqthread + 30

    Thread 3:
    0   libsystem_kernel.dylib        	0x9890b02e __workq_kernreturn + 10
    1   libsystem_c.dylib             	0x9aabcccf _pthread_wqthread + 773
    2   libsystem_c.dylib             	0x9aabe6fe start_wqthread + 30

    Thread 4:
    0   libsystem_kernel.dylib        	0x98908c22 mach_msg_trap + 10
    1   libsystem_kernel.dylib        	0x989081f6 mach_msg + 70
    2   CoreFoundation                	0x009a2b49 __CFRunLoopServiceMachPort + 185
    3   CoreFoundation                	0x009a7864 __CFRunLoopRun + 1380
    4   CoreFoundation                	0x009a6ed4 CFRunLoopRunSpecific + 276
    5   CoreFoundation                	0x009a6dab CFRunLoopRunInMode + 123
    6   Foundation                    	0x0001126c -[NSRunLoop(NSRunLoop) runMode:beforeDate:] + 298
    7   Foundation                    	0x00011135 -[NSRunLoop(NSRunLoop) run] + 82
    8   xcodebuild-crashTests         	0x01693a25 +[AFURLConnectionOperation networkRequestThreadEntryPoint:] + 133 (AFURLConnectionOperation.m:152)
    9   Foundation                    	0x0005e665 -[NSThread main] + 76
    10  Foundation                    	0x0005e5c4 __NSThread__main__ + 1304
    11  libsystem_c.dylib             	0x9aabaed9 _pthread_start + 335
    12  libsystem_c.dylib             	0x9aabe6de thread_start + 34

    Thread 5 Crashed:: com.apple.NSURLConnectionLoader
    0   CoreFoundation                	0x00982c81 CFArrayGetCount + 17
    1   Security                      	0x00ed0404 SecTrustCopyExceptions + 35
    2   CFNetwork                     	0x00c43cfa SocketStream::securityAcceptPeerTrust_NoLock(unsigned char) + 164
    3   CFNetwork                     	0x00c3ab11 SocketStream::setProperty(void const*, __CFString const*, void const*) + 355
    4   CFNetwork                     	0x00ce4b9c virtual thunk to SocketStream::setProperty(void const*, __CFString const*, void const*) + 36
    5   CFNetwork                     	0x00ce7a33 ReadStreamCallbacks::_setProperty(__CoreReadStream*, __CFString const*, void const*, void*) + 53
    6   CFNetwork                     	0x00d40477 non-virtual thunk to CoreReadStreamWithCallBacks::_streamImpl_SetProperty(__CFString const*, void const*) + 52
    7   CFNetwork                     	0x00d4223c CoreStreamBase::_streamInterface_SetProperty(__CFString const*, void const*) + 38
    8   CFNetwork                     	0x00cdf803 HTTPReadFilter::_streamImpl_SetProperty(__CFString const*, void const*) + 165
    9   CFNetwork                     	0x00cdf8e3 non-virtual thunk to HTTPReadFilter::_streamImpl_SetProperty(__CFString const*, void const*) + 34
    10  CFNetwork                     	0x00d4223c CoreStreamBase::_streamInterface_SetProperty(__CFString const*, void const*) + 38
    11  CFNetwork                     	0x00cdd319 HTTPNetStreamInfo::_streamImpl_SetProperty(__CFString const*, void const*) + 289
    12  CFNetwork                     	0x00cdd51a non-virtual thunk to HTTPNetStreamInfo::_streamImpl_SetProperty(__CFString const*, void const*) + 34
    13  CFNetwork                     	0x00d4223c CoreStreamBase::_streamInterface_SetProperty(__CFString const*, void const*) + 38
    14  CFNetwork                     	0x00ceedd7 HTTPProtocol::_protocolInterface_useCredential(_CFURLCredential const*, _CFURLAuthChallenge*) + 149
    15  CFNetwork                     	0x00d0cd9b ___loaderInterface_useCredential_block_invoke_0 + 40
    16  CFNetwork                     	0x00d0bf1c __withExistingProtocolAsync_block_invoke_0 + 30
    17  CFNetwork                     	0x00d454e3 __block_global_1 + 25
    18  CoreFoundation                	0x009a39fd CFArrayApplyFunction + 205
    19  CFNetwork                     	0x00d45c1a RunloopBlockContext::perform() + 208
    20  CFNetwork                     	0x00d45b3b non-virtual thunk to RunloopBlockContext::multiplexerClientPerform() + 20
    21  CFNetwork                     	0x00c319bd MultiplexerSource::perform() + 259
    22  CoreFoundation                	0x009850ef __CFRUNLOOP_IS_CALLING_OUT_TO_A_SOURCE0_PERFORM_FUNCTION__ + 15
    23  CoreFoundation                	0x00984b1f __CFRunLoopDoSources0 + 239
    24  CoreFoundation                	0x009a76c4 __CFRunLoopRun + 964
    25  CoreFoundation                	0x009a6ed4 CFRunLoopRunSpecific + 276
    26  CoreFoundation                	0x009a6dab CFRunLoopRunInMode + 123
    27  Foundation                    	0x00115bce +[NSURLConnection(Loader) _resourceLoadLoop:] + 393
    28  Foundation                    	0x0005e665 -[NSThread main] + 76
    29  Foundation                    	0x0005e5c4 __NSThread__main__ + 1304
    30  libsystem_c.dylib             	0x9aabaed9 _pthread_start + 335
    31  libsystem_c.dylib             	0x9aabe6de thread_start + 34

    Thread 6:
    0   libsystem_kernel.dylib        	0x9890b02e __workq_kernreturn + 10
    1   libsystem_c.dylib             	0x9aabcccf _pthread_wqthread + 773
    2   libsystem_c.dylib             	0x9aabe6fe start_wqthread + 30

    Thread 7:: com.apple.CFSocket.private
    0   libsystem_c.dylib             	0x9aad1647 tiny_free_list_remove_ptr + 127
    1   libsystem_c.dylib             	0x9aad62b1 szone_free + 956
    2   CoreFoundation                	0x0097dcb8 __CFAllocatorSystemDeallocate + 24
    3   CoreFoundation                	0x0097dc8a CFAllocatorDeallocate + 138
    4   CoreFoundation                	0x0097dae1 CFRelease + 1409
    5   CoreFoundation                	0x009e50ff __CFSocketManager + 2655
    6   libsystem_c.dylib             	0x9aabaed9 _pthread_start + 335
    7   libsystem_c.dylib             	0x9aabe6de thread_start + 34

    Thread 5 crashed with X86 Thread State (32-bit):
      eax: 0x00000000  ebx: 0x00000002  ecx: 0x00982c7d  edx: 0x05012180
      edi: 0x01735978  esi: 0x00ed03ef  ebp: 0xb0287b58  esp: 0xb0287b40
       ss: 0x00000023  efl: 0x00010282  eip: 0x00982c81   cs: 0x0000001b
       ds: 0x00000023   es: 0x00000023   fs: 0x00000023   gs: 0x0000000f
      cr2: 0x00000000
    Logical CPU: 0

    Binary Images:
        0x1000 -     0x2fff +otest (??? - ???) <1398AA4E-9571-349C-8C75-D8CDAA1AE723> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/Developer/usr/bin/otest
        0x6000 -   0x28eff7 +Foundation (985.1.0 - compatibility 300.0.0) <406DBD64-08AB-3E4F-917A-B98F2A3E8BEA> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/Foundation.framework/Foundation
      0x3dd000 -   0x3f8ffe +libSystem.dylib (125.0.0 - compatibility 1.0.0) <D46A314C-A399-39B9-8131-B44EC7D6CF4E> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/libSystem.dylib
      0x40c000 -   0x494ff7 +MobileCoreServices (39.0.0 - compatibility 1.0.0) <1FAC03CE-F013-3185-8EC1-F97040E0C3DD> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/MobileCoreServices.framework/MobileCoreServices
      0x4d1000 -   0x688ff1 +libicucore.A.dylib (49.1.0 - compatibility 1.0.0) <F34D61C9-84B4-3663-91E5-EEA25680AAD9> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/libicucore.A.dylib
      0x715000 -   0x820f8b +libobjc.A.dylib (227.0.0 - compatibility 1.0.0) <52E130A4-85BA-399F-A9AA-2F4D9CF74C46> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/libobjc.A.dylib
      0x837000 -   0x933ff7 +libxml2.2.dylib (10.8.0 - compatibility 10.0.0) <8C34E1FD-F90B-3158-9A97-D5DC923D52DC> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/libxml2.2.dylib
      0x961000 -   0x970ff7 +libz.1.dylib (1.2.5 - compatibility 1.0.0) <795F02AA-F065-3D5D-9D06-2A4E4DBC625C> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/libz.1.dylib
      0x975000 -   0xb18fff +CoreFoundation (785.0.0 - compatibility 150.0.0) <0EA0E808-1959-3DD8-9D1A-B19E71C8D5AC> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation
      0xc2c000 -   0xd99ffb +CFNetwork (592.0.0 - compatibility 1.0.0) <03430119-E704-3CA3-A4EC-FE17880E8E8F> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/CFNetwork.framework/CFNetwork
      0xe3e000 -   0xe87fff +SystemConfiguration (485.0.0 - compatibility 1.0.0) <7415EA9B-755D-30B6-ADB5-947CF77B0C05> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/SystemConfiguration.framework/SystemConfiguration
      0xea9000 -   0xef1ffb +Security (??? - ???) <2A100803-5C1C-35AE-8397-B67B1E86D22E> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/Security.framework/Security
      0xf16000 -   0xf1bffe +IOKit (275.0.0 - compatibility 1.0.0) <C0FAFF6B-73CD-35BA-9A9C-254620AEBEC1> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/IOKit.framework/IOKit
      0xf25000 -   0xf44ffb +libCRFSuite.dylib (??? - ???) <ABD29004-1887-3887-8113-DC874A563D9F> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/libCRFSuite.dylib
      0xf4e000 -   0xf4fffb +liblangid.dylib (??? - ???) <E02691B6-2660-327A-85D9-65079160FF3E> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/liblangid.dylib
      0xf53000 -   0xf64fff +GenerationalStorage (130.0.0 - compatibility 1.0.0) <93F7FC7B-83F7-3835-B87E-33F2CCCC0A47> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/GenerationalStorage.framework/GenerationalStorage
      0xf6c000 -   0xfd0ff3 +libstdc++.6.dylib (56.0.0 - compatibility 7.0.0) <5F8C495C-35F3-3D7B-AC32-27778B7DECE5> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/libstdc++.6.dylib
     0x102e000 -  0x1041ffb +libbsm.0.dylib (??? - ???) <2503C190-0D01-3D5B-B85E-D01BCA738FBC> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/libbsm.0.dylib
     0x1049000 -  0x1061ff7 +SpringBoardServices (??? - ???) <506DDC67-579F-3DA4-9B55-493595A2941D> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/SpringBoardServices.framework/SpringBoardServices
     0x1074000 -  0x1095ff7 +libc++abi.dylib (24.1.0 - compatibility 1.0.0) <263AA12F-73F3-3BCB-A2EB-AA1B5190D2FC> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/libc++abi.dylib
     0x10bf000 -  0x10bffff +libSystem.override.dylib (??? - ???) <BDC1C691-D2DF-3082-B9CC-4CB4146864F7> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/system/libSystem.override.dylib
     0x10c4000 -  0x10c8ffb +libcache_sim.dylib (56.0.0 - compatibility 1.0.0) <16B2003E-614D-3B95-AE1B-76DCB2DF2D41> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/system/libcache_sim.dylib
     0x10cd000 -  0x10dbff3 +libcommonCrypto_sim.dylib (50000.0.0 - compatibility 1.0.0) <D47B206A-E560-383E-B587-29856BF38547> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/system/libcommonCrypto_sim.dylib
     0x10e9000 -  0x1100ff3 +libdispatch.dylib (244.10.70 - compatibility 1.0.0) <02DEE3EB-27F1-3702-829E-9466C4663F07> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/system/libdispatch.dylib
     0x1111000 -  0x111bff7 +libnotify_sim.dylib (103.3.0 - compatibility 1.0.0) <F7B5D555-3090-3A95-B5C6-9A293843BE68> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/system/libnotify_sim.dylib
     0x1122000 -  0x1193fef +libsystem_sim_c.dylib (849.11.70 - compatibility 1.0.0) <1EC12455-159E-3891-B430-9F429E9392CD> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/system/libsystem_sim_c.dylib
     0x11b4000 -  0x11b5ffb +libsystem_sim_blocks.dylib (60.0.0 - compatibility 1.0.0) <3D2590F8-A972-30C6-991D-5B6D1D763C83> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/system/libsystem_sim_blocks.dylib
     0x11b9000 -  0x11c1fff +libsystem_sim_dnssd.dylib (??? - ???) <9CB45381-4752-30B5-B928-477093530114> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/system/libsystem_sim_dnssd.dylib
     0x11c7000 -  0x11e1ffb +libsystem_sim_info.dylib (??? - ???) <C65FD82C-8ECD-3FD8-87E9-6FD2788BB59C> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/system/libsystem_sim_info.dylib
     0x11ed000 -  0x121cff3 +libsystem_sim_m.dylib (??? - ???) <58DCEF5B-11E3-3AA6-BCAB-24C201E46D96> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/system/libsystem_sim_m.dylib
     0x1223000 -  0x1237ff7 +libsystem_sim_network.dylib (??? - ???) <F80004BE-4CD0-3F9A-AC41-02F86532D66B> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/system/libsystem_sim_network.dylib
     0x1244000 -  0x1266ff3 +libxpc.dylib (173.17.0 - compatibility 1.0.0) <A7498DC1-637C-33C4-9882-51D44205317F> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/system/libxpc.dylib
     0x127a000 -  0x1281ffb +libcopyfile_sim.dylib (90.1.70 - compatibility 1.0.0) <AE289D68-3DF9-349E-9120-636F753DF6CD> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/system/libcopyfile_sim.dylib
     0x1286000 -  0x1287ffb +libremovefile_sim.dylib (24.2.0 - compatibility 1.0.0) <1271E8E4-97A7-3ADF-BB3A-C70CF6626003> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/system/libremovefile_sim.dylib
     0x128c000 -  0x12cdfff +libcorecrypto_sim.dylib (??? - ???) <2DF78CF4-21B0-3CB7-A996-D5F943A6FE89> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/system/libcorecrypto_sim.dylib
     0x12d9000 -  0x12f7ffb +BackBoardServices (??? - ???) <E860233F-D6D6-3E91-B4ED-8CA87E1A81E7> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/BackBoardServices.framework/BackBoardServices
     0x1313000 -  0x1323ff7 +GraphicsServices (14.0.0 - compatibility 1.0.0) <13B0D8BA-BCDE-3E1C-BB0E-2817B68773C1> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/GraphicsServices.framework/GraphicsServices
     0x1333000 -  0x133dff3 +XPCObjects (??? - ???) <EE7558B0-910A-3E58-8DC5-3FFB6DB4FF8B> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/XPCObjects.framework/XPCObjects
     0x1348000 -  0x1518ff3  com.apple.CoreGraphics (1.600.0 - ???) <36E3E971-DD05-3058-B79F-58F24979952B> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/CoreGraphics.framework/CoreGraphics
     0x1571000 -  0x1631ff7 +libsqlite3.dylib (9.6.0 - compatibility 9.0.0) <C18B561C-E950-380E-B12C-228D98FB9E6C> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/libsqlite3.dylib
     0x1684000 -  0x16a0fff +xcodebuild-crashTests (??? - ???) <29D6B1A5-243E-3056-986E-A755ED5FAA05> /Users/USER/Library/Developer/Xcode/DerivedData/xcodebuild-crash-ftwyoapmzrqyzdfbqgsupojsmwqq/Build/Products/Debug-iphonesimulator/xcodebuild-crashTests.octest/xcodebuild-crashTests
     0x16bd000 -  0x16d6ff3 +MobileAsset (??? - ???) <CE37AB8B-B1D4-381F-8752-13BEAD425E53> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/MobileAsset.framework/MobileAsset
     0x16e6000 -  0x16edffb +libAccessibility.dylib (??? - ???) <BA2B41EE-7E8D-382F-9528-0FC9705AEA6D> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/libAccessibility.dylib
     0x16f9000 -  0x16f9ffd +Accelerate (4.0.0 - compatibility 1.0.0) <4FCBEE31-A87A-3268-B401-5A0572BB4D93> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/Accelerate.framework/Accelerate
     0x2700000 -  0x2710ffb +TelephonyUtilities (??? - ???) <2F576E4D-611F-37E2-A70E-598989E21507> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/TelephonyUtilities.framework/TelephonyUtilities
     0x2721000 -  0x2741ffb +WebBookmarks (??? - ???) <1924EBF1-5464-3884-9E2C-E53D979FFAA4> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/WebBookmarks.framework/WebBookmarks
     0x2754000 -  0x2776ffb +DictionaryServices (??? - ???) <10608061-5ACF-371C-B41D-6355BEE75075> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/DictionaryServices.framework/DictionaryServices
     0x278f000 -  0x27c5ff3 +PrintKit (104.0.0 - compatibility 1.0.0) <9E901471-3898-391A-9B6A-3D81B6490AAD> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/PrintKit.framework/PrintKit
     0x27dc000 -  0x27e7fff +libMobileGestalt.dylib (??? - ???) <B1CE8239-4B70-30B3-A6B0-684286B7ECAE> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/libMobileGestalt.dylib
     0x27f3000 -  0x27f5ffb +TCC (??? - ???) <1707A295-FA0E-3614-9C9B-928A7A2E1E51> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/TCC.framework/TCC
     0x27fa000 -  0x27fbffb +DataMigration (??? - ???) <1E946677-2752-3352-83AE-BAB3EAAB1519> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/DataMigration.framework/DataMigration
     0x5b8a000 -  0x6297ffb +UIKit (1800.0.0 - compatibility 1.0.0) <6295452C-0E80-3302-8383-FA4B5FCBB190> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/UIKit.framework/UIKit
     0x65e4000 -  0x66b0ff7 +UIFoundation (??? - ???) <3F5085A5-938C-3378-A4B4-524450CA4AC4> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/UIFoundation.framework/UIFoundation
     0x66fc000 -  0x67e7ff3 +CoreImage (2.0.0 - compatibility 1.0.0) <169499BA-10BC-3BF9-9AD0-BB3F3988F7BE> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/CoreImage.framework/CoreImage
     0x683e000 -  0x6a9fff3 +ImageIO (??? - ???) <12877ECD-0561-3FC9-9367-C5AE54230324> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/ImageIO.framework/ImageIO
     0x6b0b000 -  0x6c5eff3 +QuartzCore (1.8.0 - compatibility 1.2.0) <7BB52F53-8305-3FAB-BA02-9840936C9524> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/QuartzCore.framework/QuartzCore
     0x6ce0000 -  0x6d31fff +AppSupport (29.0.0 - compatibility 1.0.0) <0FF2A208-0925-33B1-964C-F26746ACC397> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/AppSupport.framework/AppSupport
     0x6d61000 -  0x6e0bffb +CoreText (??? - ???) <B84088A7-1D3F-39EC-892E-4D92263C9493> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/CoreText.framework/CoreText
     0x6e53000 -  0x6f6bffb +WebKit (536.12.0 - compatibility 1.0.0) <3B0507E5-F20F-3867-9E2F-5D944DEF76F2> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/WebKit.framework/WebKit
     0x7010000 -  0x845cff7 +WebCore (536.12.0 - compatibility 1.0.0) <A1C12F20-A69B-32C1-95B7-FD7B0EBA5F15> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/WebCore.framework/WebCore
     0x8c48000 -  0x8cedff7 +ProofReader (171.0.0 - compatibility 1.0.0) <65B84BF4-5D63-3132-B784-311D86FB701A> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/ProofReader.framework/ProofReader
     0x8d04000 -  0x8d67fff +libc++.1.dylib (65.0.0 - compatibility 1.0.0) <9E0B682E-27DE-36C6-8037-04A8457C4D97> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/libc++.1.dylib
     0x8db6000 -  0x8e5aff7 +CoreTelephony (1351.3.0 - compatibility 1.0.0) <B46F840C-549A-32A5-8B97-635212881B5C> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/CoreTelephony.framework/CoreTelephony
     0x8eac000 -  0x8f01ff7 +IMFoundation (800.0.0 - compatibility 1.0.0) <19BEC9CC-9382-3C0A-87D2-2EEE7A7C4C17> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/IMFoundation.framework/IMFoundation
     0x8f2b000 -  0x8f2eff7 +MobileInstallation (??? - ???) <AE579A1E-1730-35F6-9FFE-796BC3C9136C> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/MobileInstallation.framework/MobileInstallation
     0x8f33000 -  0x8f36ffb +MobileSystemServices (??? - ???) <AA9BD2F9-42D2-35FC-8DA9-670408EA1CE5> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/MobileSystemServices.framework/MobileSystemServices
     0x8f3c000 -  0x8f74ffb +Bom (189.0.0 - compatibility 2.0.0) <59B38A7E-C02A-3D60-8E31-781D18B4F63B> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/Bom.framework/Bom
     0x8f83000 -  0x8f8ffff +libbz2.1.0.dylib (1.0.5 - compatibility 1.0.0) <E1B2BAFE-50A4-3E92-89DE-AEC39AC82CB9> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/libbz2.1.0.dylib
     0x8f94000 -  0x8fc4ff3 +libtidy.A.dylib (??? - ???) <81A8363C-8AD6-3DAD-8A1B-58A00B2497FD> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/libtidy.A.dylib
     0x8fd1000 -  0x920dff3 +JavaScriptCore (536.12.0 - compatibility 1.0.0) <495BC920-49BE-3487-A782-CD1F36FB98ED> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/JavaScriptCore.framework/JavaScriptCore
     0x9289000 -  0x9295ffb +OpenGLES (??? - ???) <71DC65A4-9D5D-3CA7-B71B-38E985AC5D74> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/OpenGLES.framework/OpenGLES
     0x929f000 -  0x9439fe3 +vImage (213.3.0 - compatibility 1.0.0) <9E021D46-C470-39A0-B4CE-EE41CEB4AD1B> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/Accelerate.framework/Frameworks/vImage.framework/vImage
     0x9459000 -  0x9459ffd +vecLib (387.10.0 - compatibility 1.0.0) <2C9D77EA-9A45-3EC8-BA1E-5AE62622F121> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/Accelerate.framework/Frameworks/vecLib.framework/vecLib
     0x945d000 -  0x94caffb +libvDSP.dylib (387.10.0 - compatibility 1.0.0) <D4E1F4AF-DDB2-32D8-82AA-CDE2E021A8F4> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/Accelerate.framework/Frameworks/vecLib.framework/libvDSP.dylib
     0x94d4000 -  0x98a5ff3 +libLAPACK.dylib (??? - ???) <4F69D6EB-DA46-3174-B68A-D83168A0B4DC> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/Accelerate.framework/Frameworks/vecLib.framework/libLAPACK.dylib
     0x98d1000 -  0x9a14ff3 +libBLAS.dylib (??? - ???) <89F890E5-2BFB-3380-A98C-A695BD8A90FD> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/Accelerate.framework/Frameworks/vecLib.framework/libBLAS.dylib
     0x9a33000 -  0x9aa1fff +libvMisc.dylib (387.10.0 - compatibility 1.0.0) <E9579347-0BFF-3F63-806C-75AF6E3A07AF> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/Accelerate.framework/Frameworks/vecLib.framework/libvMisc.dylib
     0x9aa9000 -  0x9abaff7 +CoreVideo (1.8.0 - compatibility 1.2.0) <31D0044E-0128-3301-AECD-193F93D76525> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/CoreVideo.framework/CoreVideo
     0x9ac9000 -  0x9acfffb +libGFXShared.dylib (??? - ???) <6D470D60-086E-3345-9F40-6E9A22D2F413> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/OpenGLES.framework/libGFXShared.dylib
     0x9ad5000 -  0x9b15ff3 +libGLImage.dylib (??? - ???) <E68C16D7-41C7-3A2C-958A-44955B20C81A> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/OpenGLES.framework/libGLImage.dylib
     0x9b1d000 -  0x9b1ffff +libCVMSPluginSupport.dylib (??? - ???) <B8E66C32-AAAC-3265-9932-93B17E204EA9> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/OpenGLES.framework/libCVMSPluginSupport.dylib
     0x9b24000 -  0x9b2cffb +libCoreVMClient.dylib (??? - ???) <83459BA9-27A5-3321-BB6A-027FBFE5988C> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/OpenGLES.framework/libCoreVMClient.dylib
     0x9b34000 -  0xa347fe7 +libLLVMContainer.dylib (??? - ???) <0F03E1EE-33E9-3F3F-995B-04DD36E5BBC5> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/OpenGLES.framework/libLLVMContainer.dylib
     0xa687000 -  0xaaa9ff3 +FaceCoreLight (1.8.5 - compatibility 1.0.0) <35263E68-08FE-3A01-BC21-4D6F376BEA64> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/FaceCoreLight.framework/FaceCoreLight
     0xacb8000 -  0xace4ffb +libxslt.1.dylib (3.26.0 - compatibility 3.0.0) <20E7D089-E224-368F-8CE6-4C1BC37D5414> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/libxslt.1.dylib
     0xacf0000 -  0xade3fff +libiconv.2.dylib (7.0.0 - compatibility 7.0.0) <4D8FD7B1-0FBF-325C-9308-FE20A1825D66> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/libiconv.2.dylib
     0xadf0000 -  0xadf0ffd +libresolv.dylib (41.0.0 - compatibility 1.0.0) <4BB65BB0-0ED7-3BD8-AC0E-07B4FF4F4A27> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/usr/lib/libresolv.dylib
     0xadf5000 -  0xae01ffb +AssetsLibraryServices (??? - ???) <799A3E80-D444-3CF1-B3FD-668AAFDA8AB9> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/AssetsLibraryServices.framework/AssetsLibraryServices
     0xaed1000 -  0xaedcff3 +IAP (??? - ???) <28C130E5-2BFF-3C59-9500-5E45600C115D> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/IAP.framework/IAP
     0xaee8000 -  0xaef2ff3 +MediaRemote (??? - ???) <BE2D34D4-1E13-3792-AEE6-8BECD96DAB79> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/MediaRemote.framework/MediaRemote
     0xb000000 -  0xb233ff3 +CoreData (407.2.0 - compatibility 1.0.0) <DA33A1A9-C90C-37B1-AAC2-46D08B75DC96> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/CoreData.framework/CoreData
     0xb2ef000 -  0xb2f2ffb +CommonUtilities (800.0.0 - compatibility 1.0.0) <7D9CF1A2-9B43-31A3-8366-1B6CB8181DEA> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/CommonUtilities.framework/CommonUtilities
     0xb4af000 -  0xb4f8ffb +libCGFreetype.A.dylib (600.0.0 - compatibility 64.0.0) <0F6EF8EF-EA5D-3F13-BBF0-FD986DC820B5> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/CoreGraphics.framework/Resources/libCGFreetype.A.dylib
     0xb50c000 -  0xb56cffb +QuickLook (??? - ???) <E5CB8784-FEC8-3260-8126-86C1367468AA> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/QuickLook.framework/QuickLook
     0xb5a6000 -  0xb6f1ffb +AVFoundation (2.0.0 - compatibility 1.0.0) <2CD70C73-AC21-3E0E-A4BA-4F60F3270B75> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/AVFoundation.framework/AVFoundation
     0xb7b2000 -  0xb7ccff3 +PersistentConnection (??? - ???) <AEF80453-C182-3D9E-9341-C96879F03AEB> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/PersistentConnection.framework/PersistentConnection
     0xf606000 -  0xf79dff7 +MediaPlayer (??? - ???) <84D5C07D-21EC-3CE9-B014-4F344A54C4CB> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/MediaPlayer.framework/MediaPlayer
     0xf89c000 -  0xf90dff7 +ManagedConfiguration (??? - ???) <70A1552B-925D-33AB-9D37-162A3062A2B6> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/ManagedConfiguration.framework/ManagedConfiguration
     0xf951000 -  0xf99cfff +iTunesStore (??? - ???) <4318622C-8927-3F95-A456-07337D71FCA6> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/iTunesStore.framework/iTunesStore
     0xf9cf000 -  0xfa0dff7 +Celestial (??? - ???) <5CE1533D-5943-3DDA-87B7-4ED20A87DEFE> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/Celestial.framework/Celestial
     0xfa3b000 -  0xfb07ff3 +MusicLibrary (18.0.0 - compatibility 1.0.0) <BB072A14-5EE4-376C-BC4B-9FBDA16E73C2> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/MusicLibrary.framework/MusicLibrary
     0xfb65000 -  0xfbcbffb +CoreMedia (??? - ???) <01CD8A40-E3F8-3F25-B273-06E87A3572A6> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/CoreMedia.framework/CoreMedia
     0xfbfc000 -  0xfc9eff3 +StoreServices (??? - ???) <19AD9B03-55C2-39D3-BABB-589E694A5194> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/StoreServices.framework/StoreServices
     0xfd0b000 - 0x1004bffb +AudioToolbox (359.0.0 - compatibility 1.0.0) <8E42A769-8D33-3A81-A250-5949F50DD125> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/AudioToolbox.framework/AudioToolbox
    0x1010d000 - 0x10140ffb +HomeSharing (??? - ???) <DE1F55B9-50AA-392D-887B-F6F1A0C56F31> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/HomeSharing.framework/HomeSharing
    0x10162000 - 0x1019cff7 +MediaControlSender (??? - ???) <1F109303-0FF1-36CF-A38F-DBB2D41EEE95> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/PrivateFrameworks/MediaControlSender.framework/MediaControlSender
    0x101b2000 - 0x10248ff3 +CoreMotion (1458.2.0 - compatibility 1.0.0) <65882646-F2B6-35B0-8692-A3D639EAC338> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/CoreMotion.framework/CoreMotion
    0x1029b000 - 0x10319ffb +CoreAudio (??? - ???) <5BAEBF45-6044-317C-ADB8-ECCA648CC576> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/CoreAudio.framework/CoreAudio
    0x10351000 - 0x10668ff3 +VideoToolbox (??? - ???) <73FCB7BB-5377-353E-93F7-EAA5E7416A55> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/VideoToolbox.framework/VideoToolbox
    0x106d4000 - 0x10997fff +MediaToolbox (??? - ???) <7E6C6D0D-1E24-3C1D-812D-9642ECADA27C> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/System/Library/Frameworks/MediaToolbox.framework/MediaToolbox
    0x20100000 - 0x2010cfff +SenTestingKit (3460.0.0 - compatibility 1.0.0) <381B83E6-8BE1-37C2-9CEE-6310B0697799> /Applications/Xcode45-DP1.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk/Developer/Library/Frameworks/SenTestingKit.framework/SenTestingKit
    0x8fe22000 - 0x8fe54aa7  dyld (195.6 - ???) <3A866A34-4CDD-35A4-B26E-F145B05F3644> /usr/lib/dyld
    0x90773000 - 0x90778ff7  libmacho.dylib (800.0.0 - compatibility 1.0.0) <943213F3-CC9B-328E-8A6F-16D85C4274C7> /usr/lib/system/libmacho.dylib
    0x90779000 - 0x9077aff0  libunc.dylib (24.0.0 - compatibility 1.0.0) <2F4B35B2-706C-3383-AA86-DABA409FAE45> /usr/lib/system/libunc.dylib
    0x9267a000 - 0x9267dff7  libcompiler_rt.dylib (6.0.0 - compatibility 1.0.0) <7F6C14CC-0169-3F1B-B89C-372F67F1F3B5> /usr/lib/system/libcompiler_rt.dylib
    0x92956000 - 0x9295aff3  libsystem_network.dylib (??? - ???) <62EBADDA-FC72-3275-AAB3-5EDD949FEFAF> /usr/lib/system/libsystem_network.dylib
    0x92ca2000 - 0x92ca3ff7  libquarantine.dylib (36.6.0 - compatibility 1.0.0) <600909D9-BD75-386E-8D3E-7CBD29079DF3> /usr/lib/system/libquarantine.dylib
    0x93b73000 - 0x93b77ffa  libcache.dylib (47.0.0 - compatibility 1.0.0) <56256537-6538-3522-BCB6-2C79DA6AC8CD> /usr/lib/system/libcache.dylib
    0x95310000 - 0x95318ff3  libunwind.dylib (30.0.0 - compatibility 1.0.0) <E8DA8CEC-12D6-3C8D-B2E2-5D567C8F3CB5> /usr/lib/system/libunwind.dylib
    0x957c7000 - 0x957f6ff7  libsystem_info.dylib (??? - ???) <37640811-445B-3BB7-9934-A7C99848250D> /usr/lib/system/libsystem_info.dylib
    0x95ac5000 - 0x95acdff3  liblaunch.dylib (392.38.0 - compatibility 1.0.0) <D7F6E875-263A-37B5-B403-53F76710538C> /usr/lib/system/liblaunch.dylib
    0x9860f000 - 0x98616ff7  libsystem_notify.dylib (80.1.0 - compatibility 1.0.0) <47DB9E1B-A7D1-3818-A747-382B2C5D9E1B> /usr/lib/system/libsystem_notify.dylib
    0x98742000 - 0x98743fff  libsystem_blocks.dylib (53.0.0 - compatibility 1.0.0) <B04592B1-0924-3422-82FF-976B339DF567> /usr/lib/system/libsystem_blocks.dylib
    0x988f2000 - 0x98910ff7  libsystem_kernel.dylib (1699.26.8 - compatibility 1.0.0) <3705DE40-E00F-3E37-ADB0-D4AE5F9950F5> /usr/lib/system/libsystem_kernel.dylib
    0x98911000 - 0x98975fff  com.apple.framework.IOKit (2.0 - ???) <88D60E59-430D-35B8-B1E9-F5138301AEF9> /System/Library/Frameworks/IOKit.framework/Versions/A/IOKit
    0x98976000 - 0x98977ff7  libsystem_sandbox.dylib (??? - ???) <EBC6ED6B-7D94-32A9-A718-BB9EDA1732C9> /usr/lib/system/libsystem_sandbox.dylib
    0x98a4c000 - 0x98a7afe7  libSystem.B.dylib (159.1.0 - compatibility 1.0.0) <30189C33-6ADD-3142-83F3-6114B1FC152E> /usr/lib/libSystem.B.dylib
    0x98d95000 - 0x98d95fff  libdnsinfo.dylib (395.11.0 - compatibility 1.0.0) <7EFAD88C-AFBC-3D48-BE14-60B8EACC68D7> /usr/lib/system/libdnsinfo.dylib
    0x990b9000 - 0x990bbff7  libdyld.dylib (195.6.0 - compatibility 1.0.0) <1F865C73-5803-3B08-988C-65B8D86CB7BE> /usr/lib/system/libdyld.dylib
    0x990bc000 - 0x990c3ff9  libsystem_dnssd.dylib (??? - ???) <D3A766FC-C409-3A57-ADE4-94B7688E1C7E> /usr/lib/system/libsystem_dnssd.dylib
    0x99193000 - 0x991b0fff  libresolv.9.dylib (46.1.0 - compatibility 1.0.0) <2870320A-28DA-3B44-9D82-D56E0036F6BB> /usr/lib/libresolv.9.dylib
    0x9a58f000 - 0x9a5d2ffd  libcommonCrypto.dylib (55010.0.0 - compatibility 1.0.0) <6B35F203-5D72-335A-A4BC-CC89FEC0E14F> /usr/lib/system/libcommonCrypto.dylib
    0x9a5fd000 - 0x9a605ff5  libcopyfile.dylib (85.1.0 - compatibility 1.0.0) <BB0C7B49-600F-3551-A460-B7E36CA4C4A4> /usr/lib/system/libcopyfile.dylib
    0x9aa5e000 - 0x9ab29fff  libsystem_c.dylib (763.13.0 - compatibility 1.0.0) <52421B00-79C8-3727-94DE-62F6820B9C31> /usr/lib/system/libsystem_c.dylib
    0x9ad84000 - 0x9ad8ffff  libkxld.dylib (??? - ???) <D8ED88D0-7153-3514-9927-AF15A12261A5> /usr/lib/system/libkxld.dylib
    0x9bbeb000 - 0x9bbecff4  libremovefile.dylib (21.1.0 - compatibility 1.0.0) <6DE3FDC7-0BE0-3791-B6F5-C15422A8AFB8> /usr/lib/system/libremovefile.dylib
    0x9c764000 - 0x9c767ff7  libmathCommon.A.dylib (2026.0.0 - compatibility 1.0.0) <69357047-7BE0-3360-A36D-000F55E39336> /usr/lib/system/libmathCommon.A.dylib
    0x9c768000 - 0x9c768ffe  libkeymgr.dylib (23.0.0 - compatibility 1.0.0) <7F0E8EE2-9E8F-366F-9988-E2F119DB9A82> /usr/lib/system/libkeymgr.dylib

    External Modification Summary:
      Calls made by other processes targeting this process:
        task_for_pid: 4
        thread_create: 0
        thread_set_state: 0
      Calls made by this process:
        task_for_pid: 0
        thread_create: 0
        thread_set_state: 0
      Calls made by all processes on this machine:
        task_for_pid: 115499567
        thread_create: 2
        thread_set_state: 651998

    VM Region Summary:
    ReadOnly portion of Libraries: Total=169.1M resident=70.9M(42%) swapped_out_or_unallocated=98.2M(58%)
    Writable regions: Total=87.8M written=2512K(3%) resident=7296K(8%) swapped_out=0K(0%) unallocated=80.7M(92%)
     
    REGION TYPE                      VIRTUAL
    ===========                      =======
    MALLOC                             72.7M
    MALLOC guard page                    48K
    SQLite page cache                   192K
    Stack                              67.1M
    VM_ALLOCATE                          44K
    __DATA                             8472K
    __LINKEDIT                         69.2M
    __PAGEZERO                            4K
    __TEXT                             99.9M
    __UNICODE                           544K
    mapped file                       283.9M
    shared memory                        12K
    shared pmap                        13.8M
    ===========                      =======
    TOTAL                             615.7M